<%--
  Created by IntelliJ IDEA.
  User: adm
  Date: 06.12.2016
  Time: 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>demis task</title>
    <style type="text/css">
        .error {color: red;}
        .hide {display: none;}
        p.error {
            display: block;
            margin-top: 6px;
            text-align: center;
        }

        .text-center {
            text-align: center;
        }
        .block {
            width: 650px;
            height: 650px;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: center;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

    </style>
</head>

<body>

<div class="block">
    <form name="textsPost" method="post" action="/demis">
        <p class="text-center">
            <label class="${error1}">Текст 1 *</label>
            <input type="text" name="text_1" size="65" value="${text1}" maxlength="1000">
            <p class="hide ${error1}">Не заполнено обязательное поле "текст 1"</p>
        </p>
        <p class="text-center">
            <label class="${error2}">Текст 2 *</label>
            <input type="text" name="text_2" size="65" value="${text2}" maxlength="1000">
            <p class="hide ${error2}">Не заполнено обязательное поле "текст 2"</p>
        </p>
        <p class="text-center">
            <label class="${error3}">Текст 3 *</label>
            <input type="text" name="text_3" size="65" value="${text3}" maxlength="1000">
            <p class="hide ${error3}">Не заполнено обязательное поле "текст 3"</p>
        </p>
        <p class="text-center">
            <label class="${error4}">Текст 4 *</label>
            <input type="text" name="text_4" size="65" value="${text4}" maxlength="1000">
            <p class="hide ${error4}">Не заполнено обязательное поле "текст 4"</p>
        </p>
        <p class="text-center">
            <label class="${error5}">Текст 5 *</label>
            <input type="text" name="text_5" size="65" value="${text5}" maxlength="1000">
            <p class="hide ${error5}">Не заполнено обязательное поле "текст 5"</p>
        </p>
        <p class="text-center">
            <input type="submit" value="Поиск пересечений">
        </p>

        <table>
            <tr>
                <th>Слово</th>
                <th>Количество текстов</th>
                <th>Номера текстов</th>
            </tr>

            ${htmlResult}

        </table>

        <%--test fields--%>
        <%--<p class="text-center">
            <input type="text" name="testOutput" size="55" value="${testOutput1}">
            <input type="text" name="testOutput" size="55" value="${testOutput2}">
            <input type="text" name="testOutput" size="55" value="${testOutput3}">
            <input type="text" name="testOutput" size="55" value="${testOutput4}">
            <input type="text" name="testOutput" size="55" value="${testOutput5}">
            <input type="text" name="testOutput" size="55" value="${testOutput6}">
            <input type="text" name="testOutput" size="55" value="${testOutput7}">
        </p>--%>


    </form>
</div>

</body>
</html>
