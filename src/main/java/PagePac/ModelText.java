package PagePac;
/**
 * Created by adm on 06.12.2016.
 */

import java.util.ArrayList;
import java.util.regex.*;
import static java.util.regex.Pattern.UNICODE_CHARACTER_CLASS;


public class ModelText {

    private String text;
    private boolean textIsValid;
    private ArrayList<String> arrayStrings = new ArrayList<String>();

    //construct
    public ModelText(String text) {

        this.text = text;
        this.textIsValid = false;
    }

    //getters
    public String getText() {
        return text;
    }
    public boolean isTextIsValid() {
        return textIsValid;
    }
    public ArrayList<String> getArrayStrings() {
        return arrayStrings;
    }


    //setters
    public void setTextIsValid(boolean textIsValid) {
        this.textIsValid = textIsValid;
    }
    public void setText(String text) {
        this.text = text;
    }


    //method for split text
    public void splitText(){
        Pattern regexP = Pattern.compile("\\b\\w{3,}\\b", UNICODE_CHARACTER_CLASS);
        Matcher m = regexP.matcher(this.getText());
        while (m.find()) {
            if(!this.arrayStrings.contains(m.group().toLowerCase())) {
                this.arrayStrings.add(m.group().toLowerCase());
            }
        }
    }
}
