package PagePac;

import java.sql.*;
/**
 * Created by adm on 06.12.2016.
 */
public class JDBC {

    private static final String url = "jdbc:mysql://localhost:3306/demistaskdb?useUnicode=true&characterEncoding=UTF-8";

    private static final String user = "root";
    private static final String password = "";

    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    //method for writing into DB
    static void dataInput(String word, String theNumberOfTexts, String textNumbers) {
        //forming query
        String queryInputRow = "INSERT INTO demistaskdb.results (word, the_number_of_texts, text_numbers)"
                + " VALUES ('" + word + "', '" + theNumberOfTexts + "', '" + textNumbers +"');";

        //Register JDBC driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            //opening db connection to MySQL server
            con = DriverManager.getConnection(url, user, password);

            //getting Statement object to execute query
            stmt = con.createStatement();

            //executing update query
            stmt.executeUpdate(queryInputRow);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) { /*can't do anything*/ }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se) { /*can't do anything*/ }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException se) { /*can't do anything*/ }
        }
    }
}
