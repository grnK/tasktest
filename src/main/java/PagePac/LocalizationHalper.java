package PagePac;

import java.nio.charset.StandardCharsets;

/**
 * Created by adm on 09.12.2016.
 */
public class LocalizationHalper {
    public static String getPost(String post) {
        String returnedPost = post;
        byte[] bytes = returnedPost.getBytes(StandardCharsets.ISO_8859_1);
        returnedPost = new String(bytes, StandardCharsets.UTF_8);
        return returnedPost;
    }
}
