package PagePac;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by adm on 05.12.2016.
 */
@WebServlet("/demis")
public class ChiefPage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("ChiefJSP.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ModelText text1 = new ModelText(LocalizationHalper.getPost(req.getParameter("text_1")));
        ModelText text2 = new ModelText(LocalizationHalper.getPost(req.getParameter("text_2")));
        ModelText text3 = new ModelText(LocalizationHalper.getPost(req.getParameter("text_3")));
        ModelText text4 = new ModelText(LocalizationHalper.getPost(req.getParameter("text_4")));
        ModelText text5 = new ModelText(LocalizationHalper.getPost(req.getParameter("text_5")));

        if(Validator.badVal(text1)) {
            req.setAttribute("error1", "error");
            req.setAttribute("text1", text1.getText());
        } else { req.setAttribute("text1", text1.getText()); }

        if(Validator.badVal(text2)) {
            req.setAttribute("error2", "error");
            req.setAttribute(text2.getText(), "text2");
        } else { req.setAttribute("text2", text2.getText()); }

        if(Validator.badVal(text3)) {
            req.setAttribute("error3", "error");
            req.setAttribute(text3.getText(), "text3");
        } else { req.setAttribute("text3", text3.getText()); }

        if(Validator.badVal(text4)) {
            req.setAttribute("error4", "error");
            req.setAttribute(text4.getText(), "text4");
        } else { req.setAttribute("text4", text4.getText()); }

        if(Validator.badVal(text5)) {
            req.setAttribute("error5", "error");
            req.setAttribute(text5.getText(), "text5");
        } else { req.setAttribute("text5", text5.getText()); }

        if(text1.isTextIsValid() && text2.isTextIsValid() && text3.isTextIsValid()
                && text4.isTextIsValid() && text5.isTextIsValid()) {


            //Создаём массивы слов
            text1.splitText();
            text2.splitText();
            text3.splitText();
            text4.splitText();
            text5.splitText();

            Analysis analysis = new Analysis();
            analysis.allIntersection(analysis.createBigArray(text1, text2, text3, text4, text5));
            analysis.prepareAfter();

            //TESTS
/*
            req.setAttribute("testOutput1", text1.getArrayStrings());
            req.setAttribute("testOutput2", text2.getArrayStrings());
            req.setAttribute("testOutput3", analysis.getNumTexts());
            req.setAttribute("testOutput4", analysis.getWords());
            req.setAttribute("testOutput5", analysis.getResultString());
            req.setAttribute("testOutput6", analysis.printResultTree());
            req.setAttribute("testOutput7", testEncodingMyPost);
*/

            //Формируем таблицу
            req.setAttribute("htmlResult", analysis.htmlResult());
        }

        req.getRequestDispatcher("ChiefJSP.jsp").forward(req, resp);


    }
}
