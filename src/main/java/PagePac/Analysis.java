package PagePac;

import java.util.*;


public class Analysis {

    private ArrayList<String> words = new ArrayList<String>();
    private ArrayList<String> numTexts = new ArrayList<String>();
    private ArrayList<String> resultString = new ArrayList<String>();

    //Хэш массив результатов
    private HashMap<String, ArrayList<Integer>> resultsHash = new HashMap<String, ArrayList<Integer>>();

    //Новый хэш
    private TreeMap<String, ArrayList<Integer>> resultTree = new TreeMap<String, ArrayList<Integer>>();


    //getter
    public HashMap<String, ArrayList<Integer>> getResultsHash() {
        return resultsHash;
    }
    public ArrayList<String> getWords() {
        return words;
    }
    public ArrayList<String> getNumTexts() {
        return numTexts;
    }
    public ArrayList<String> getResultString() {
        return resultString;
    }

    //Обработка после формирования хэшмапа
    public void prepareAfter() {
        Integer countTexts;
        ArrayList<Integer> textsArray = new ArrayList<Integer>();

        Set set = resultsHash.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry mapentry = (Map.Entry)iterator.next();
            words.add(mapentry.getKey().toString());
            numTexts.add(mapentry.getValue().toString());
            resultString.add(mapentry.getKey().toString() + mapentry.getValue());


            textsArray = (ArrayList<Integer>) mapentry.getValue();
            countTexts = textsArray.size();

            resultTree.put(
                    (5 - countTexts) + "::" + mapentry.getKey().toString(),
                    (ArrayList<Integer>) mapentry.getValue()
            );
        }
        //Запись в БД
        this.sqlResult();
    }

    //Формирование тестовой строки для вывода Дерева
    public String printResultTree() {
        String out ="";
        Set set = resultTree.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry mapentry = (Map.Entry)iterator.next();
            out = (out + mapentry.getKey().toString() + mapentry.getValue());
        }
        return out;
    }

    //Формирование строки в HTML для вывода в таблицу 20 значений
    public String htmlResult() {
        ArrayList<Integer> textsArray = new ArrayList<Integer>();
        String stringArray = "";
        String out ="";
        int exit = 0;
        Set set = resultTree.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            if (exit > 19){
                break;
            }

            Map.Entry mapentry = (Map.Entry)iterator.next();
            textsArray = (ArrayList<Integer>) mapentry.getValue();
            stringArray =  textsArray.toString();
            out = out + "<tr>";
            out = (out + "<td>" + mapentry.getKey().toString().substring(3) + "</td>");
            out = (out + "<td>" + textsArray.size() + "</td>");
            out = (out + "<td>" + stringArray.replace(",", ";").replace("[", "").replace("]", "") + "</td>");
            out = out + "</tr>";

            exit++;
        }
        return out;
    }

    //Формирование результатов для записи в БД
    public void sqlResult() {
        ArrayList<Integer> textsArray = new ArrayList<Integer>();
        String stringArray = "";
        Set set = resultTree.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry mapentry = (Map.Entry)iterator.next();

            textsArray = (ArrayList<Integer>) mapentry.getValue();
            stringArray =  textsArray.toString();

            JDBC.dataInput(mapentry.getKey().toString().substring(3),
                    textsArray.size() + "",
                    stringArray.replace(",", ";").replace("[", "").replace("]", "")
            );
        }
    }


    //Поиск пересечений слов из двух массивов
    public ArrayList<String> intersection(ArrayList<String> first, ArrayList<String> second){
        ArrayList<String> firstArray = new ArrayList<String>(first);
        ArrayList<String> secondArray = new ArrayList<String>(second);

        firstArray.retainAll(secondArray);
        return  firstArray;
    }

    //Создаём массив масивов
    public ArrayList<ArrayList<String>> createBigArray(ModelText t1, ModelText t2, ModelText t3, ModelText t4,
                                                              ModelText t5){
        ArrayList<ArrayList<String>> returningBigArray = new ArrayList<ArrayList<String>>();
        returningBigArray.add(t1.getArrayStrings());
        returningBigArray.add(t2.getArrayStrings());
        returningBigArray.add(t3.getArrayStrings());
        returningBigArray.add(t4.getArrayStrings());
        returningBigArray.add(t5.getArrayStrings());
        return returningBigArray;
    }

    //Ищем все пересечения в текстах
    public  HashMap<String, ArrayList<Integer>> allIntersection(ArrayList<ArrayList<String>> bigArrayList) {

        for (int i = 0; i < bigArrayList.size() - 1; i++) {

            for (int j = i + 1; j < bigArrayList.size(); j++) {


                for(String word : this.intersection(bigArrayList.get(i), bigArrayList.get(j))) {
                    if(resultsHash.containsKey(word)) {
                        ArrayList<Integer> items = resultsHash.get(word);
                        if(!items.contains(j + 1)) {
                            items.add(j + 1);
                            resultsHash.put(word, items);
                        }
                    } else {
                        ArrayList<Integer> numbersOfTexts = new ArrayList<Integer>();
                        numbersOfTexts.add(i + 1);
                        numbersOfTexts.add(j + 1);
                        resultsHash.put(word, numbersOfTexts);
                    }
                }
            }
        }
        return resultsHash;
    }
}
